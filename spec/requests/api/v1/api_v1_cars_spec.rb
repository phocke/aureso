require 'rails_helper'

RSpec.describe 'Api::V1::Cars', type: :request do

  let(:json) { JSON.parse(response.body)["data"] }

  let(:car){ create :car }
  let(:asphalt_track){ create :track }


  describe 'GET /api/v1/cars/:slug' do

    context 'non existent car' do
      it 'responds with 404' do
        get '/api/v1/cars/no_such_car.json'

        expect(response.content_type).to eq('application/json')
        expect(response).to have_http_status(404)
      end
    end

    context 'no track param' do
      it 'responds with 200' do
        get api_v1_car_path({format: 'json', slug: car.slug})

        expect(response).to have_http_status(200)
        expect(json['max_speed_on_track']).to eq('no track selected')
      end
    end

    context 'no track found' do
      it 'responds with 200' do
        get api_v1_car_path({format: 'json', slug: car.slug, track: 'no_such_track'})

        expect(response).to have_http_status(200)
        expect(json['max_speed_on_track']).to eq('track not found')
      end
    end

    context 'real track' do
      it 'responds with 200' do
        get api_v1_car_path({format: 'json', slug: car.slug, track: asphalt_track.slug})

        expect(response).to have_http_status(200)
        expect(json['max_speed']).to eq("#{car.max_speed}km/h")
        expect(json['max_speed_on_track']).to eq("224.4km/h")
      end
    end
  end
end
