require 'rails_helper'

RSpec.describe Car, type: :model do
  let(:car){ build :car }
  let(:track){ build :track }
  let(:simple_processor){ SimpleMaxSpeedProcessor.new(car) }
  let(:advanced_processor){ AdvancedMaxSpeedProcessor.new(car) }

  describe '.max_speed_on_track' do
    it "handles null track" do
      expect(car.max_speed_on_track).to be_zero
    end

    it "works with any calculator" do
      expect(car.max_speed_on_track track: track, processor: simple_processor)
        .to_not be_zero

      expect(car.max_speed_on_track track: track, processor: advanced_processor)
        .to_not be_zero
    end
  end
end
