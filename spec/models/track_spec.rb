require 'rails_helper'

RSpec.describe Track, type: :model do
  let(:european_track){ build :track }
  let(:asian_track){ build :track, utc_offset: 8.hours.to_i }
  let(:expected_time){ Time.new(2017, 1, 1, 0, 0, 0, "+02:00") }

  describe '.current_time' do
    before do
      Timecop.freeze(expected_time) # midnight CET
    end

    after do
      Timecop.return
    end

    it "returns correct time" do
      expect(european_track.current_time).to eq(expected_time)
      expect(asian_track.current_time.strftime('%R')).to eq("06:00")
    end
  end
end
