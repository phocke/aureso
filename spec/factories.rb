FactoryGirl.define do
  factory :car do
    slug "subaru_impreza"
    max_speed 255
  end

  factory :track do
    name 'Hockenheim ring'
    slug 'hockenheim'
    surface 'asphalt'
    utc_offset 2.hours.to_int #seconds
  end
end
