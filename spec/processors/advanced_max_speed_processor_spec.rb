require 'rails_helper'

RSpec.describe AdvancedMaxSpeedProcessor do
  let(:car){ build :car }
  let(:null_track){ NullTrack.new }
  let(:asphalt_track){ build :track }
  let(:snow_track){ build(:track, surface: 'snow') }
  let(:processor){ AdvancedMaxSpeedProcessor.new(car, null_track) }

  describe '.calculate' do
    it 'works with null track' do
      expect(processor.calculate).to be_zero
    end

    it 'works with real track' do
      processor.track = asphalt_track
      expect(processor.calculate).to_not be_zero
    end

    context "optimal time" do
      before do
        Timecop.freeze(Time.new(2017, 1, 1, 15, 0, 0, "+02:00")) # 3pm CET
      end

      after do
        Timecop.return
      end

      it 'for asphalt' do
        processor.track = asphalt_track
        expect(processor.calculate).to eql(224.4) # 255 * (1-0.12) = 224.4
      end

      it 'for snow' do
        processor.track = snow_track
        expect(processor.calculate).to eql(165.75) # 255 * (1-0.35) = 165.75
      end
    end

    context "slow time" do
      before do
        Timecop.freeze(Time.new(2017, 1, 1, 0, 0, 0, "+02:00")) # midnight CET
      end

      after do
        Timecop.return
      end

      it 'for asphalt' do
        processor.track = asphalt_track
        expect(processor.calculate).to eql(190.74) # 255 * (1-0.15) * (1-0.12) = 190.74
      end

      it 'for snow' do
        processor.track = snow_track
        expect(processor.calculate).to eql(140.8875) # 255 * (1-0.35) * (1-0.15) = 140.8875
      end
    end
  end
end
