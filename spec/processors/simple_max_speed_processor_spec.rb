require 'rails_helper'

RSpec.describe SimpleMaxSpeedProcessor do
  let(:car){ build :car }
  let(:null_track){ NullTrack.new }
  let(:asphalt_track){ build :track }
  let(:snow_track){ build(:track, surface: 'snow') }
  let(:processor){ SimpleMaxSpeedProcessor.new(car, null_track) }

  describe '.calculate' do
    it 'works with null track' do
      expect(processor.calculate).to be_zero
    end

    it 'works with real track' do
      processor.track = asphalt_track
      expect(processor.calculate).to_not be_zero
    end

    it 'for asphalt' do
      processor.track = asphalt_track
      expect(processor.calculate).to eql(224.4) # 255 * (1-0.12) = 224.4
    end

    it 'for snow' do
      processor.track = snow_track
      expect(processor.calculate).to eql(165.75) # 255 * (1-0.35) = 165.75
    end
  end
end
