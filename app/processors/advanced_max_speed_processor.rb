class AdvancedMaxSpeedProcessor < SimpleMaxSpeedProcessor

  def calculate
    super * (1.0 - time_slowing_factor)
  end

private
  def time_slowing_factor
    time = @track.current_time.seconds_since_midnight.to_i

    # 9am – 6pm => 0%
    # 6pm – 9:30pm => 8%
    # 9:30pm – 6am => 15%
    # 6am – 9am => 8%

    case time
    when (0)..(6.hours.to_int) then 0.15
    when (6.hours.to_int.succ)..(9.hours.to_int.succ) then 0.08
    when (9.hours.to_int.succ)..(18.hours.to_int) then 0
    when (18.hours.to_int.succ)..(21.hours.to_int + 30.minutes.to_int) then 0.08
    when (21.hours.to_int + 30.minutes.to_int.succ)..(24.hours.to_int) then 0.15
    end
  end
end
