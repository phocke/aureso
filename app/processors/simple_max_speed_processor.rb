class SimpleMaxSpeedProcessor

  SURFACE_SLOWING_FACTORS = {
    'snow' => 0.35,
    'asphalt' => 0.12,
    'gravel' => 0.20
  };  SURFACE_SLOWING_FACTORS.default = 1;  SURFACE_SLOWING_FACTORS.freeze

  attr_accessor :car, :track, :slowing_factor

  def initialize car, track=NullTrack.new
    @car = car
    @track = track
  end

  def calculate
    @car.max_speed * (1.0 - slowing_factor)
  end

private
  def slowing_factor
    @slowing_factor ||= SURFACE_SLOWING_FACTORS[@track.surface]
  end
end
