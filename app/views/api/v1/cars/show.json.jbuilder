json.data do
  json.partial! "car", car: @car

  if !params[:track]
    json.max_speed_on_track 'no track selected'
  elsif  @track.is_a?(NullTrack)
    json.max_speed_on_track 'track not found'
  else
    json.max_speed_on_track @car.max_speed_on_track(track: @track).to_s.concat("km/h")
  end
end
