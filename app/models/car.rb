class Car < ApplicationRecord
  validates :slug, uniqueness: true

  def max_speed_on_track(track: NullTrack.new, processor: SimpleMaxSpeedProcessor.new(self))
    processor.track = track
    processor.calculate
  end
end
