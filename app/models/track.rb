class Track < ApplicationRecord
  AvailableSurfaces = ['snow', 'gravel', 'asphalt']

  validates :surface, inclusion: AvailableSurfaces

  def current_time
    Time.now.getlocal(self.utc_offset)
  end
end
