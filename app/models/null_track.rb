class NullTrack < Track
  def name;"";end
  def slug; ""; end
  def surface; ""; end
  def utc_offset; 0; end
end
