class Api::V1::CarsController < ApplicationController

  def show
    @car = Car.find_by(slug: params[:slug])
    head :not_found unless @car

    @track = Track.find_by(slug: params[:track]) || NullTrack.new
  end

end
