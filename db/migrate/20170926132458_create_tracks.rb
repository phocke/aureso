class CreateTracks < ActiveRecord::Migration[5.1]
  def change
    create_table :tracks do |t|
      t.integer :utc_offset, null: false
      t.string :surface, null: false
      t.string :name, null: false
      t.string :slug, null: false

      t.timestamps
    end
  end
end
