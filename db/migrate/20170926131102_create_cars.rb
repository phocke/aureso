class CreateCars < ActiveRecord::Migration[5.1]
  def change
    create_table :cars do |t|
      t.string :slug, null: false
      t.integer :max_speed, null: false

      t.timestamps
    end
  end
end
