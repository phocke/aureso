# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

tracks_data = [
  {
    slug: 'nurburgring',
    name: 'Nurburgring',
    utc_offset: 1.hour.to_int,
    surface: 'asphalt'
  },
  {
    slug: 'sydney',
    name: 'Sydney Motorsport Park',
    utc_offset: 10.hours.to_int,
    surface: 'asphalt'
  },
  {
    slug: 'montreal',
    name: 'Circuit Gilles Villenaeuve Montreal',
    utc_offset: -5.hours.to_int,
    surface: 'asphalt'
  },
  {
    slug: 'guia',
    name: 'Guia Circut',
    utc_offset: 8.hours.to_int,
    surface: 'asphalt'
  },
]

car_data = [
  {
    slug: 'subaru_impreza',
    max_speed: 270
  },
  {
    slug: 'mitsubishi_lancer',
    max_speed: 278
  },
  {
    slug: 'fiat_126p',
    max_speed: 130
  },
  {
    slug: 'bugatti_chiron',
    max_speed: 463
  }
]


tracks_data.each do |track|
  Track.create! track
end

car_data.each do |car|
  Car.create! car
end
