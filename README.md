# README

I'm using sqlite3, so to bootstrap the project you just need to run

    bundle
    rake db:create && rake db:migrate && rake db:seed
    rails s

To run specs use RSpec as usual:

    bundle exec rspec

I highly recommend installing [httpie](https://github.com/jakubroztocil/httpie) instead of curl or wget in case you're planning on using the project locally.

To test app locally this is how you can do it with httpie:

    http localhost:3000/api/v1/cars/subaru_impreza.json?track=nurburgring

This is the expected response:

    HTTP/1.1 200 OK
    Cache-Control: max-age=0, private, must-revalidate
    Content-Type: application/json; charset=utf-8
    ETag: W/"1cfc165cb6c205f425c7be49c1cff369"
    Transfer-Encoding: chunked
    X-Request-Id: b7fa8d8a-b364-4f20-b3aa-f0aaa0d8e767
    X-Runtime: 0.032353

    {
        "data": {
            "id": 1,
            "max_speed": "270km/h",
            "max_speed_on_track": "237.6km/h",
            "slug": "subaru_impreza"
        }
    }


## Important things I've noticed while working on this assignment


### TimeZone

  For simplicity sake, I used `utc_offset` rather than timezone for `Track` model, as there was no point doing so for a project this simple. If this was a real-world project I'd need to take care of a daylight saving hours issue. As specified in the description I've set the timezone for this project to CET in `application.rb`

### Slug

  The description of the assignment specifies that a full name of a track should be used for finding tracks. I think that would constitute a bad API design as that would make it unnecessarily complicated to construct a simple request. Think of escaping whitespace and other related issues.


  In the description of the assignment there's a structure where the JSON output would look something like `{"car":{"car_slug": "whatever"}` which is considered a bad naming practice, so I just used `slug` instead.

### Number of seed tracks

  The description of the assignment specifies that there should be 3 tracks seeded to the database and then there's data provided for 4 tracks. Because of that, I decided to seed 4 tracks.

### Response uniformity

  The description of the assignment specifies that possible values for `max_speed_for_track` attribute should be one of the following `['track not found', 'no track selected', Number]`. If this was a real-world project I would opt for `[-1, 0, Number]` correspondingly, as this would keep the type of the attribute uniform, regardless of the actual case. I would also document this parameter in the API docs.

### Max speed calculation

  Since the max speed calculation is fairly simple and we're dealing with relatively small numbers, I opted for using good old `Int` for those calculations. This makes us prone to rounding errors, but for calculations this basic, this can be safely ignored.

### Keeping stuff decoupled

  To keep the modules nicely decoupled, I've opted for dependency injection, which will let me easily swap out the class that will be responsible for those calculations in the future. This will make any potential refactoring a breeze. To make my life a bit easier I also used null object pattern to avoid using excessive conditionals in the code related to Track class.

### Spec design

  Since `SimpleMaxSpeedProcessor` and `AdvancedMaxSpeedProcessor` classes are fairly similar I was considering using RSpec's shared examples group, but for a case this small this would be an overkill in my opinion.

